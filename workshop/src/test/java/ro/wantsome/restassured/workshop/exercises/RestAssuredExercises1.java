package ro.wantsome.restassured.workshop.exercises;

import io.restassured.RestAssured;
import org.junit.BeforeClass;
import org.junit.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class RestAssuredExercises1 {

    @BeforeClass
    public static void initPath() {

        RestAssured.baseURI = "http://localhost:9876";
    }

    /*******************************************************
     * Send a GET request to /api/companies/1/jobs
     * and check that the answer has HTTP status code 200
     ******************************************************/

    @Test
    public void checkResponseCodeForCorrectRequest() {

        given().
                when().get("/api/companies/1/jobs").
                then();
    }

    /*******************************************************
     * Send a GET request to /api/companies/incorrect
     * and check that the answer has HTTP status code 500
     ******************************************************/

    @Test
    public void checkResponseCodeForIncorrectRequest() {

        given().
                when().
                then();
    }

    /*******************************************************
     * Send a GET request to /api/companies/1/jobs
     * and check that the response is in JSON format
     ******************************************************/

    @Test
    public void checkResponseContentTypeJson() {

        given().
                when().
                then();
    }

    /***********************************************
     * Retrieve information for the first job
     * of company Google and check the level equals
     * senior
     * Use /api/companies/1/jobs
     **********************************************/

    @Test
    public void checkTheFirstJobForGoogleHasLevelSenior() {

        given().
                when().
                then();
    }

    /***********************************************
     * Retrieve the list of jobs for the company
     * Google and check that it contains Java
     * Use /api/companies/1/jobs
     **********************************************/

    @Test
    public void checkThereIsAJobAtGoogleWithJava() {

        given().
                when().
                then();
    }

    /***********************************************
     * Retrieve the list of jobs for the company
     * Google and check that it doesn't contain
     * ruby
     * Use /api/companies/1/jobs
     **********************************************/

    @Test
    public void checkThereIsNoRubyRelatedJobAtGoogle() {

        given().
                when().
                then();
    }
}
