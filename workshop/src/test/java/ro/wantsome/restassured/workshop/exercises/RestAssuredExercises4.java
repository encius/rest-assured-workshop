package ro.wantsome.restassured.workshop.exercises;

import io.restassured.RestAssured;
import org.junit.BeforeClass;
import org.junit.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class RestAssuredExercises4 {

    /*******************************************************
     * Create a ResponseSpecification that checks whether:
     * - the response has statusCode 200
     * - the response contentType is JSON
     ******************************************************/
    @BeforeClass
    public static void initPathAndcreateResponseSpecification() {

        RestAssured.baseURI = "http://localhost:9876";
    }

    /*******************************************************
     * Request an authentication token for
     * API and write the response to the console
     * Use Basic authentication
     * username = wantsome
     * password = wantsome
     * Use /api/oauth2/token
     * Store this authentication token in a String variable
     * for future reference
     ******************************************************/

    String accessToken;

    @Test
    public void retrieveOAuthToken() {

                given().
                        when().
                        then();
    }

    /*******************************************************
     * Get private jobs
     * Use OAuth2 authentication with the previously stored
     * authentication token.
     * Use /api/jobs/private
     ******************************************************/

    @Test
    public void getPrivateJobs() {

        given().
                when().
                then();
    }

    /*******************************************************
     * Retrieve the jobs for company Google
     * Use the previously created ResponseSpecification to
     * execute the specified checks
     * Use /api/companies/1/jobs
     * Additionally, check that the level is Senior
     ******************************************************/

    @Test
    public void useResponseSpecification() {

        given().
                when().
                then();
    }

    /*******************************************************
     * Create a new Java model for the 404 request response
     * Use GET call on /api/jobs/4
     * Make use of model to perform response validations
     ******************************************************/

    @Test
    public void jsonModelTest() {

        given().
                when().
                then();
    }
}