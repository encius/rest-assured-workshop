package ro.wantsome.restassured.workshop.exercises;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.ResponseSpecification;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

@RunWith(DataProviderRunner.class)
public class RestAssuredExamples {

    @Test
    public void testCompanyNameForId() {
        given().when()
                .get("http://localhost:9876/api/companies/1")       // Perform a GET call to the specified resource
                .then().log().all().and().assertThat()                                   // Check that the value of the element 'name'
                .body("companies.name", equalTo("Google"));          // in the response body equals 'Google'
    }

    @Test
    public void testResponseHeaders() {
        given().when().
                get("http://localhost:9876/api/companies/1").
                then().assertThat().
                statusCode(200).
                and().
                contentType("application/json");

    }

    @Test
    public void useSingleQueryParameter() {

        given().
                queryParam("active", "true").
                when().
                get("http://localhost:9876/api/jobs").
                then()
                .body("jobs[1].company", equalTo("Wantsome"));
    }

    @Test
    public void useSinglePathParameter() {

        given().
                pathParam("countryCode", "ie").
                when().
                get("http://localhost:9876/api/jobs/{countryCode}").
                then()
                .body("jobs[0].company", equalTo("Google"));
    }

    @Test
    public void useMultiplePathParameter() {
        given().
                pathParam("companyId", "1").
                pathParam("jobId", "1").
                when().
                get("http://localhost:9876/api/companies/{companyId}/jobs/{jobId}").
                then().
                body("salary", equalTo(4000));
    }

    @Test
    public void deleteJob() {
        given().
                when().
                delete("http://localhost:9876/api/jobs/tester").
                then().
                assertThat().
                statusCode(401);
    }

    @Test
    public void basicAuth() {
        given().
                auth().
                preemptive().basic("wantsome", "wantsome").
                when().
                delete("http://localhost:9876/api/companies/1").
                then().
                assertThat().
                statusCode(200);
    }

    @Test
    public void oAuth2() {
        given().
                auth().
                oauth2("MTQ0NjJkZmQ5OTM2NDE1ZTZjNGZmZjI3").
                when().
                get("http://localhost:9876/api/companies/2").
                then().
                assertThat().
                statusCode(200);
    }

    @Test
    public void checkResponseTime() {
        given().when().
                get("http://localhost:9876/api/companies").
                then().
                time(lessThan(200L), TimeUnit.MILLISECONDS);
    }

    @Test
    public void extractValueFromResponse() {
        String jobDescription = given().when().
                get("http://localhost:9876/api/companies/1/jobs/1").
                then().
                extract().path("description");
        System.out.println(jobDescription);
    }

    @Test
    public void responseSpecificationTest() {
        ResponseSpecBuilder builder = new ResponseSpecBuilder();
        builder.expectStatusCode(404);
        builder.expectContentType(ContentType.JSON);
        ResponseSpecification responseSpec = builder.build();

        when().get("http://localhost:9876/api/jobs/4").
                then().
                spec(responseSpec).and().
                body("message", equalTo("Job not found"));
    }

    @Test
    public void postRequestBodyTest() {
        JobModel jobModel = new JobModel();
        jobModel.setCompany("Microsoft");
        jobModel.setPosition(".NET Developer");
        jobModel.setSalary(5000);

        given().contentType(ContentType.JSON)
                .body(jobModel)
                .post("http://localhost:9876/api/jobs")
                .then().log().body();
    }

    @DataProvider
    public static Object[][] createFilteredJobsData() {

        return new Object[][] {
                { "true", 2 },
                { "false", 1 }
        };
    }

    @Test
    @UseDataProvider("createFilteredJobsData")
    public void checkJobCountWhenFilteredByStatus(String status, int total) {

        given().
                queryParam("active", status).
                when().
                get("http://localhost:9876/api/companies/1/jobs").
                then().
                assertThat().
                body("total", is(total));
    }


}