package ro.wantsome.restassured.workshop.exercises;

import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class RestAssuredExercises3 {

    @BeforeClass
    public static void initPath() {

        RestAssured.baseURI = "http://localhost:9876";
    }

    /*******************************************************
     * Request an authentication token API and write the
     * response to the console. Use preemptive Basic authentication:
     * username = wantsome
     * password = wantsome
     * Use /api/oauth2/token
     ******************************************************/

    @Before
    public void retrieveOAuthToken() {

        given().
                when().
                then();
    }

    /*******************************************************
     * Request a list of private jobs using above token and check
     * that there is a job at NASA
     * Use OAuth2 authentication with the previously retrieved
     * authentication token.
     * Use /api/jobs/private
     ******************************************************/

    @Test
    public void checkPrivateJobAtNASA() {

        given().
                when().
                then();
    }

    /*******************************************************
     * Request the list of all jobs for company with ID 1
     * and check that this request is answered within 100 ms
     * Use /api/companies/1
     ******************************************************/

    @Test
    public void checkResponseTimeForCompaniesList() {

        given().
                when().
                then();
    }

}