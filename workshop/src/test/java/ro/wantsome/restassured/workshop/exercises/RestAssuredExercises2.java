package ro.wantsome.restassured.workshop.exercises;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import io.restassured.RestAssured;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class RestAssuredExercises2 {

    @BeforeClass
    public static void initPath() {

        RestAssured.baseURI = "http://localhost:9876";
    }

    /*******************************************************
     * Create a DataProvider that specifies the following
     * job details: country code and salary
     ******************************************************/



    /*******************************************************
     * Create a DataProvider that specifies the following
     * company details: compant ID, country and city
     ******************************************************/



    /*******************************************************
     * Request data for country specific
     * jobs and check the salaries
     * Use /api/jobs/{countryCode} with pathParam
     ******************************************************/

    @Test
    public void checkSalariesForEachJobBasedOnCountryCode() {

        given().
                when().
                then();
    }

    /*******************************************************
     * Request data for companies using company ID
     * and check the country and city for each company
     * Use /api/companies/{companyId} with pathParam
     ******************************************************/

    @Test
    public void checkCountryAndCityForEachCompanyBasedOnCompanyId() {

        given().
                when().
                then();
    }

}